from flask import Flask, jsonify, request
import json
import os
from datetime import datetime, timedelta

app = Flask(__name__)

# Define the opening and closing times
opening_time = datetime.strptime('08:30', '%H:%M').time()
closing_time = datetime.strptime('22:30', '%H:%M').time()

# Load or initialize the data from the JSON file
data_file = 'data.json'
if os.path.exists(data_file):
    with open(data_file, 'r') as f:
        data = json.load(f)
else:
    data = {'booked': 0, 'open': 10}

# Helper function to update the data file
def update_data_file():
    with open(data_file, 'w') as f:
        json.dump(data, f)

# Endpoint to get available time slots
@app.route('/backend/available_times', methods=['GET'])
def get_available_times():
    now = datetime.now().time()
    available_times = []

    # Calculate the time slots from the current time until closing time
    current_time = datetime.strptime(str(now), '%H:%M:%S.%f').time()
    delta = timedelta(minutes=30)
    current_slot = datetime.strptime(str(current_time), '%H:%M:%S').time()
    while current_slot < closing_time:
        if current_slot >= opening_time:
            available_times.append(datetime.strftime(current_slot, '%H:%M'))
        current_slot = (datetime.combine(datetime.min, current_slot) + delta).time()

    return jsonify(available_times)

# Endpoint to book an appointment
@app.route('/backend/book_appointment', methods=['POST'])
def book_appointment():
    global data

    # Get the form data
    form_data = request.json

    # Get the selected services and their durations
    services = form_data.get('services', [])
    total_duration = sum([service.get('duration', 0) for service in services])

    # Check if the total duration exceeds the closing time
    if total_duration > (closing_time.hour * 60 + closing_time.minute):
        return jsonify({'success': False, 'message': 'The selected services exceed the closing time (10:30pm). Please choose a different combination.'})

    # Update the data
    data['booked'] += 1
    data['open'] -= 1

    # Save the updated data to the file
    update_data_file()

    return jsonify({'success': True, 'message': 'Appointment booked successfully.'})

# Endpoint to get the booked services and open times
@app.route('/backend/appointment_data', methods=['GET'])
def get_appointment_data():
    global data
    return jsonify(data)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)

