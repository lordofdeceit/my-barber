from flask import Flask, jsonify, request
import json
import os
from datetime import datetime, timedelta

app = Flask(__name__)

# Define the opening and closing times
opening_time = datetime.strptime('08:30', '%H:%M').time()
closing_time = datetime.strptime('22:30', '%H:%M').time()

# Define the duration of each service in minutes
service_durations = {
    'head_shave': 40,
    'scrubbing': 30,
    'massage': 90,
    'beards': 30,
    'dye': 40,
}

# Load or initialize the data from the JSON file
data_file = 'data.json'
if os.path.exists(data_file):
    with open(data_file, 'r') as f:
        data = json.load(f)
else:
    data = {'booked': [], 'open': []}

# Helper function to update the data file
def update_data_file():
    with open(data_file, 'w') as f:
        json.dump(data, f)

# Helper function to check if a time slot is available for a given set of services
def is_available(time_slot, services):
    # Check if any of the services are already booked for this time slot
    for service in services:
        if any(booking['time'] == time_slot and booking['service'] == service for booking in data['booked']):
            return False

    # Check if any of the services overlap with existing bookings
    for service in services:
        duration = service_durations[service]
        start_time = datetime.strptime(time_slot, '%H:%M')
        end_time = (start_time + timedelta(minutes=duration)).time()
        if any(booking['service'] == service and datetime.strptime(booking['time'], '%H:%M').time() < end_time for booking in data['booked']):
            return False

    # The time slot is available for all services
    return True

# Endpoint to get available time slots for a given set of services
@app.route('/backend/available_times', methods=['GET'])
def get_available_times():
    services = request.args.getlist('services')

    # Calculate the available time slots based on the services and other conditions
    now = datetime.now().time()
    available_times = []

    # Calculate the time slots from the current time until closing time
    current_time = now.replace(second=0, microsecond=0)
    delta = timedelta(minutes=30)
    current_slot = current_time
    while current_slot < closing_time:
        if current_slot >= opening_time and is_available(current_slot.strftime('%H:%M'), services):
            available_times.append(current_slot.strftime('%H:%M'))
        current_slot = (datetime.combine(datetime.min, current_slot) + delta).time()

    return jsonify(available_times)


# Endpoint to book an appointment
@app.route('/backend/book_appointment', methods=['POST'])
def book_appointment():
    global data

    # Get the form data
    form_data = request.json

    # Get the selected time and services
    time_slot = form_data.get('time')
    services = form_data.get('services', [])

    # Check if all services are available for the selected time slot
    if not is_available(time_slot, services):
        return jsonify({'success': False, 'message': 'One or more of the selected services are not available for the selected time slot. Please choose a different time or combination of services.'})

    # Update the data with the new bookings
    for service in services:
        data['booked'].append({'time': time_slot, 'service': service})

    # Save the updated data to the file
    update_data_file()

    return jsonify({'success': True, 'message': 'Appointment booked successfully.'})

# Endpoint to get the booked services and open times
@app.route('/backend/appointment_data', methods=['GET'])
def get_appointment_data():
    global data

    # Calculate open times based on booked times and service durations
    open_times = []
    current_time = opening_time
    delta = timedelta(minutes=30)
    while current_time < closing_time:
        if is_available(current_time.strftime('%H:%M'), service_durations.keys()):
            open_times.append(current_time.strftime('%H:%M'))
        current_time = (datetime.combine(datetime.min, current_time) + delta).time()

    return jsonify({'booked': data['booked'], 'open': open_times})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)

