from flask import Flask, request, jsonify
from flask_cors import CORS
import json


app = Flask(__name__)
CORS(app)

# Load data from data.json file
with open('data.json', 'r') as f:
    data = json.load(f)

# Route for fetching available time slots
@app.route('/available-time-slots', methods=['POST'])
def available_time_slots():
    # Get selected services from request data
    services = request.json['services']

    # TODO: Implement logic to check availability of selected services and return available time slots

    # Example response data
    time_slots = ['9:00 AM', '10:00 AM', '11:00 AM']
    return jsonify({'timeSlots': time_slots})

# Route for booking appointment
@app.route('/book-appointment', methods=['POST'])
def book_appointment():
    # Get selected services and time slot from request data
    services = request.json['services']
    time_slot = request.json['timeSlot']

    # TODO: Implement logic to book appointment and update availability

    # Example response data
    chart_data = {
        'Head Shave': [1, 2, 3],
        'Scrubbing': [2, 3, 4],
        'Massage': [1, 1, 2],
        'Beards': [3, 2, 1],
        'Dye': [2, 2, 2]
    }
    return jsonify({'chartData': chart_data})

if __name__ == '__main__':
    app.run()

