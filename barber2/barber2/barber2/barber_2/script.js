// Get references to DOM elements
const bookingForm = document.querySelector('#booking-form');
const timeSlotsDiv = document.querySelector('#time-slots');
const chartDiv = document.querySelector('#chart');

// Handle form submission
bookingForm.addEventListener('submit', event => {
  event.preventDefault();

  // Get selected services
  const services = [];
  const serviceCheckboxes = document.querySelectorAll('input[name=services]:checked');
  serviceCheckboxes.forEach(checkbox => {
    services.push(checkbox.value);
  });

  // Get selected time slot
  const timeSlotRadio = document.querySelector('input[name=time-slot]:checked');
  let timeSlot = null;
  if (timeSlotRadio) {
    timeSlot = timeSlotRadio.value;
  }

  // Send data to backend server to book appointment
  fetch('http://localhost:5000/book-appointment', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      services,
      timeSlot
    })
  })
  .then(response => response.json())
  .then(data => {
    // Update availability chart
    updateChart(data.chartData);
  });
}); // <-- Add closing brace here

// Fetch available time slots from backend server
function fetchTimeSlots(services) {
  fetch('/available-time-slots', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ services })
  })
  .then(response => response.json())
  .then(data => {
    // Update time slots in DOM
    updateTimeSlots(data.timeSlots);
  });
}

// Update time slots in DOM
function updateTimeSlots(timeSlots) {
  // Clear existing time slots
  timeSlotsDiv.innerHTML = '';

  // Add new time slots
  timeSlots.forEach(timeSlot => {
    const label = document.createElement('label');
    label.innerHTML = `
      <input type="radio" name="time-slot" value="${timeSlot}">
      ${timeSlot}
    `;
    timeSlotsDiv.appendChild(label);
    timeSlotsDiv.appendChild(document.createElement('br'));
  });
}

// Update availability chart in DOM
function updateChart(chartData) {
  // TODO: Implement chart rendering using chartData
}

