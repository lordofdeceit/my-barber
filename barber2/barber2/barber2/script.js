// Function to fetch available time slots from the backend API
async function fetchAvailableTimeSlots(services) {
  try {
    const response = await fetch(`http://127.0.0.1:5000/backend/available_times?services=${services.join('&services=')}`);
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }
    const data = await response.json();
    return data;
  } catch (error) {
    console.error('Error fetching available time slots:', error);
    return [];
  }
}

// Function to book an appointment via the backend API
async function bookAppointment(formData) {
  try {
    const response = await fetch('http://127.0.0.1:5000/backend/book_appointment', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData),
    });
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }
    const data = await response.json();
    return data;
  } catch (error) {
    console.error('Error booking appointment:', error);
    return { success: false, message: 'Error booking appointment. Please try again later.' };
  }
}

// Function to display available time slots in the form
async function displayAvailableTimeSlots() {
  const timeSelect = document.getElementById('time');
  const availabilityAlert = document.getElementById('availability');

  // Get the selected services
  const serviceCheckboxes = document.querySelectorAll('input[name="service"]:checked');
  const services = Array.from(serviceCheckboxes).map((checkbox) => checkbox.value);

  // Fetch available time slots
  const availableTimes = await fetchAvailableTimeSlots(services);
  if (availableTimes.length === 0) {
    timeSelect.innerHTML = '<option value="" disabled>No available time slots</option>';
    availabilityAlert.classList.remove('alert-success');
    availabilityAlert.classList.add('alert-danger');
    availabilityAlert.textContent = 'Sorry, no available time slots at the moment. Please try again later.';
  } else {
    // Display available time slots in the select dropdown
    timeSelect.innerHTML = '<option value="" disabled selected>Select a time</option>';
    availableTimes.forEach((time) => {
      const option = document.createElement('option');
      option.value = time;
      option.textContent = time;
      timeSelect.appendChild(option);
    });
    availabilityAlert.classList.remove('alert-danger');
    availabilityAlert.classList.add('alert-success');
    availabilityAlert.textContent = 'Available time slots loaded successfully.';
  }
}

// Function to check if a service is available at a given time
function isAvailable(time, services) {
  const timeWithoutSeconds = time.substring(0, 5); // Remove seconds from time format
  return !services.some((service) => service.time === timeWithoutSeconds);
}

// Function to display the chart with booked services and open times
async function displayChart() {
  const chartCanvas = document.getElementById('chart');
  const ctx = chartCanvas.getContext('2d');

  // Fetch booked services and open times
  const response = await fetch('http://127.0.0.1:5000/backend/appointment_data');
  if (!response.ok) {
    console.error('Error fetching appointment data');
    return;
  }
  const data = await response.json();

  const { booked, open } = data;

  // Calculate the number of booked services and open times for each service
  const serviceLabels = ['head_shave', 'scrubbing', 'massage', 'beards', 'dye'];
  const bookedServicesData = serviceLabels.map((service) => booked.filter((booking) => booking.service === service).length);
  const openTimesData = serviceLabels.map((service) => open.filter((time) => isAvailable(time, [service])).length);

  // Your code for chart creation goes here...
}

// Function to handle form submission and book the appointment
async function book() {
  const bookingForm = document.getElementById('booking-form');
  const formData = new FormData(bookingForm);

  const selectedServices = Array.from(formData.getAll('service'));

  // Calculate the total duration of selected services
  const totalDuration = selectedServices.reduce((total, service) => {
    const duration = parseInt(service.dataset.duration, 10);
    return total + duration;
  }, 0);

  // Check if the total duration exceeds the shop's closing time (10:30pm)
  const closingTime = 22 * 60 + 30; // 10:30pm in minutes since midnight
  if (totalDuration > closingTime) {
    alert('The selected services exceed the closing time (10:30pm). Please choose a different combination.');
    return;
  }

  formData.append('services', selectedServices);

  // Book the appointment
  const bookingResult = await bookAppointment(formData);
  if (bookingResult.success) {
    alert('Appointment booked successfully!');
    bookingForm.reset();
    displayAvailableTimeSlots();
    displayChart();
  } else {
    alert(bookingResult.message);
  }
}

// Function to initialize the page
async function init() {
  // Display available time slots in the form
  displayAvailableTimeSlots();

  // Display the chart with booked services and open times
  displayChart();

  // Add event listener to the form submission
  const bookingForm = document.getElementById('booking-form');
  bookingForm.addEventListener('submit', function (event) {
    event.preventDefault();
    book();
  });
}

// Call the init function when the page is loaded
document.addEventListener('DOMContentLoaded', init);






