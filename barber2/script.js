// Function to check appointment availability
function checkAvailability() {
  const timeInput = document.getElementById('time');
  const availability = document.getElementById('availability');
  
  // Check if time is available
  if (timeInput.value === '14:00') {
    availability.textContent = 'Available';
    availability.classList.remove('unavailable');
    availability.classList.add('available');
  } else {
    availability.textContent = 'Unavailable';
    availability.classList.remove('available');
    availability.classList.add('unavailable');
  }
}

// Add event listener to the time input
document.getElementById('time').addEventListener('change', checkAvailability);

// Function to handle form submission
function handleFormSubmission(event) {
  event.preventDefault();
  
  // Get form values
  const nameInput = document.getElementById('name');
  const timeInput = document.getElementById('time');
  const successMessage = document.getElementById('success-message');
  const errorMessage = document.getElementById('error-message');
  
  // Validate form inputs
  if (nameInput.value.trim() === '' || timeInput.value === '') {
    errorMessage.textContent = 'Please fill in all fields.';
    successMessage.textContent = '';
    return;
  }
  
  // Perform form submission logic
  // Here, you can send the form data to the server for further processing
  
  // Display success message
  successMessage.textContent = 'Appointment booked successfully!';
  errorMessage.textContent = '';
  
  // Reset form fields
  nameInput.value = '';
  timeInput.value = '';
}

// Add event listener to the form
document.querySelector('.appointment-form').addEventListener('submit', handleFormSubmission);

