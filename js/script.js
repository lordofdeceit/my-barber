
var button = document.querySelector(".btn");

// A function that defines a behavior or an action
function changeColor() {
    // A statement that changes the color of the button
    button.style.color = "red";
}

// An event listener that attaches a function to an event, such as click, mouseover, keypress, etc.
button.addEventListener("click", changeColor);
